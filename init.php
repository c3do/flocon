<?php declare(strict_types=1);
/* Copyright (C) 2024 Cédric Ancelin <cedric@ancode.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

define('TRANSLATIONS', [
    'en' => 'en_US',
    'fr' => 'fr_FR',
]);

define('BASE_DIR', __DIR__ . '/');

// define('BASE_URI', (empty($_SERVER['SERVER_PORT']) || '80' === $_SERVER['SERVER_PORT'] ? '' : ":{$_SERVER['SERVER_PORT']}")
//     . (
//         empty($_SERVER['CONTEXT_PREFIX']) ?
//         substr(str_replace('\\', '/', realpath(__DIR__)), strlen($_SERVER['DOCUMENT_ROOT']))
//         : $_SERVER['CONTEXT_PREFIX'] . substr(str_replace('\\', '/', realpath(__DIR__)), strlen($_SERVER['CONTEXT_DOCUMENT_ROOT']))
//     )
// );

// define('BASE_URL',
//     (
//         (isset($_SERVER['HTTPS']) && ('on' === $_SERVER['HTTPS'] || 1 === $_SERVER['HTTPS']))
//             || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && 'https' === $_SERVER['HTTP_X_FORWARDED_PROTO'])
//         ? 'https://' : 'http://'
//     )
//     . (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'])
//     . BASE_URI
// );

// var_dump(BASE_URL);


// ==============================================================================
// PATH
// ==============================================================================

if (isset($_SERVER['PATH_INFO'])) {
    $PATH_INFO = explode('/', $_SERVER['PATH_INFO']);
}


// ------------------------------------------------------------------------------
// LOCALE / TRANSLATION
// ------------------------------------------------------------------------------

if (isset($PATH_INFO[1]) && isset(TRANSLATIONS[$PATH_INFO[1]])) {
    define('LOCALE_LANG', TRANSLATIONS[$PATH_INFO[1]]);
    define('ISO_LANG', strtolower($PATH_INFO[1]));
    define('LOCALE', isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']) : LOCALE_LANG);
}
else {
    $lang = 'en';

    if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
        $browser_lang = locale_get_primary_language(locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']));

        if (in_array($browser_lang, array_keys(TRANSLATIONS))) {
            $lang = $browser_lang;
        }
    }

    header("Location: {$_SERVER['SCRIPT_NAME']}/{$lang}" . ($_SERVER['PATH_INFO'] ?? ''));
    exit;
}

// for windows compatibility (e.g. xampp) : theses 2 lines are useless for linux systems
putenv('LANG=' . LOCALE_LANG . '.utf8');
putenv('LANGUAGE=' . LOCALE_LANG . '.utf8');

// set locale
setlocale(LC_ALL, LOCALE_LANG . '.utf8');

// set domain
bindtextdomain('main', BASE_DIR . 'locale');
bind_textdomain_codeset('main', 'UTF-8'); // for windows compatibility (e.g. xampp), useless for linux systems
textdomain('main');


// ==============================================================================
// CONFIG
// ==============================================================================

if (file_exists(BASE_DIR . 'config.ini')) {
    $flConf = parse_ini_file(BASE_DIR . 'config.ini', true);
}


// ==============================================================================
// ERROR HANDLER
// ==============================================================================

if (isset($flConf['debug']) && true === $flConf['debug']) {
    ini_set('display_errors', 'on');
}

error_reporting(E_ALL);

function error_handler($ex)
{
    http_response_code(500);

    if (filter_var(ini_get('display_errors'), FILTER_VALIDATE_BOOLEAN))
    {
        die(
            '<p><span style="color:red;font-weight:bold;">' . $ex->getMessage() . '</span>'
            . ' in ' . $ex->getFile()
            . ' <span style="font-weight:bold;">line ' . $ex->getLine() . '</span><br>'
            . $ex->getTraceAsString() . '</p>'
        );
    }

    else
    {
        error_log(PHP_EOL . $ex->getMessage() . ' in ' . $ex->getFile() . ' line ' . $ex->getLine() . PHP_EOL . $ex->getTraceAsString());
        die(
            '<h1>500 Internal Server Error</h1>An internal server error has been occurred.<br>Please try again later.'
        );
    }
}

set_exception_handler('error_handler');

set_error_handler(function ($level, $message, $file = '', $line = 0)
{
    throw new ErrorException($message, 0, $level, $file, $line);
});

register_shutdown_function(function ()
{
    $error = error_get_last();
    if ($error !== null) {
        $ex = new ErrorException(
            $error['message'], 0, $error['type'], $error['file'], $error['line']
        );
        error_handler($ex);
    }
});


// ==============================================================================
// INSTALL
// ==============================================================================

if (!isset($flConf)) {
    require BASE_DIR . 'install/install.php';
    exit;
}


// ==============================================================================
// SESSION
// ==============================================================================

//session_start();
//$_SESSION['BACK'] = $_SESSION['BACK'] ?? get_path('index.php');




// ==============================================================================
// DB
// ==============================================================================

//$flDb = new PDO(
//    "{$flConf['database']['driver']}:host={$flConf['database']['host']}"
//    . (empty($flConf['database']['port']) ? '' : ";port={$flConf['database']['port']}")
//    . ";dbname={$flConf['database']['name']}",
//    $flConf['database']['username'],
//    $flConf['database']['password'],
//    [
//        PDO::ATTR_PERSISTENT => true,
//    ]
//);


// ==============================================================================
// FUNCTIONS
// ==============================================================================

function get_path(string $filename = '')
{
    return BASE_URI . "/public/{$filename}/" . ISO_LANG;
}

//function url(string $path = '')
//{
//    return APP_URL . BASE_URI . "/$path/" . ISO_LANG;
//}


//
//function a(array $fl)
//{
//    $GLOBALS['tmp'] = $fl;
//}
//
//function a(array $fl)
//{
//    $GLOBALS['tmp'] = $fl;
//}
//
//function b(array $fl)
//{
//    $GLOBALS['tmp'] = $fl['conf1'];
//}
//
//function c(array $fl_conf)
//{
//    $GLOBALS['tmp'] = $fl_conf;
//}
//
//$fl['conf1'] = $fl_conf;
//$fl['conf2'] = $fl_conf;
//$fl['conf3'] = $fl_conf;
//$fl['conf4'] = $fl_conf;
//$fl['conf5'] = $fl_conf;
//$fl['conf6'] = $fl_conf;
//
//$time = microtime(true);
//for ($i = 0; $i < 10000; ++$i) {
//    $tmp = null;
//    a($fl);
//}
//var_dump(microtime(true) - $time);
//
//$time = microtime(true);
//for ($i = 0; $i < 10000; ++$i) {
//    $tmp = null;
//    b($fl);
//}
//var_dump(microtime(true) - $time);
//
//$time = microtime(true);
//for ($i = 0; $i < 10000; ++$i) {
//    $tmp = null;
//    c($fl_conf);
//}
//var_dump(microtime(true) - $time);

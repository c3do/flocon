<?php declare(strict_types=1);
/* Copyright (C) 2024 Cédric Ancelin <cedric@ancode.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Retrieve the item from the database using its id
 *
 * @param int $id
 *
 * @return array The desired item
 */
function get_organization(int $id)
{

}

/**
 * Retrieve the item from the database using its id
 *
 * @param array $organization
 *
 * @return array The saved item
 */
function save_organization(array $organization)
{

}

/**
 * Delete the item from the database using its id
 *
 * @param array $organization
 *
 * @return bool true if successful
 */
function delete_organization(array $organization)
{

}

/**
 * Validates the contents of the data table
 *
 * @param array $organization
 *
 * @return bool true if successful
 */
function is_organization(array $organization): bool
{

}
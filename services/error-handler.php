<?php declare(strict_types=1);
/* Copyright (C) 2024 Cédric Ancelin <cedric@ancode.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

if (isset($fl_conf['debug']) && true === $fl_conf['debug']) {
    ini_set('display_errors', 'on');
}

error_reporting(E_ALL);

function error_handler($e)
{
    http_response_code(500);

    if (filter_var(ini_get('display_errors'), FILTER_VALIDATE_BOOLEAN))
    {
        die(
            '<p><span style="color:red;font-weight:bold;">' . $e->getMessage() . '</span>'
            . ' in ' . $e->getFile()
            . ' <span style="font-weight:bold;">line ' . $e->getLine() . '</span><br>'
            . $e->getTraceAsString() . '</p>'
        );
    }

    else
    {
        error_log(PHP_EOL . $e->getMessage() . ' in ' . $e->getFile() . ' line ' . $e->getLine() . PHP_EOL . $e->getTraceAsString());
        die(
            '<h1>500 Internal Server Error</h1>
              An internal server error has been occurred.<br>
              Please try again later.'
        );
    }
}

set_exception_handler('error_handler');

set_error_handler(function ($level, $message, $file = '', $line = 0)
{
    throw new ErrorException($message, 0, $level, $file, $line);
});

register_shutdown_function(function ()
{
    $error = error_get_last();
    if ($error !== null) {
        $e = new ErrorException(
            $error['message'], 0, $error['type'], $error['file'], $error['line']
        );
        error_handler($e);
    }
});
